# Awesome Browsers

A curated list of awesome browsers.

## Webkit

- [Falkon] -- *KDE web browser using QtWebEngine rendering engine, previously known as QupZilla.*
- [Konqueror] -- *KDE file manager and web browser.*
- [Qutebrowser] -- *Keyboard-focused browser with a minimal GUI.*

### WebKit2GTK+

- [Epiphany] -- *Simple, clean, beautiful view of the web.* Basically GNOME's take on Safari, it's a lightweight browser with support for [Firefox Sync](https://www.mozilla.org/en-US/firefox/features/sync/).
- [Lariza] -- *Simple WebKit2GTK+ Browser.*
- [Luakit] -- *Fast, extensible, and customizable web browser.* Like Vim and Lynx had a baby who somehow escaped the terminal.
- [Surf] -- *Simple web browser based on WebKit2/GTK+.* Tiny browser that requires editing C to make config changes. Not very accessible, but fast and maintainable.
- [Surfer] -- *Simple keyboard based web browser. No tabs.* Another small browser like Surf, but rougher around the edges than I'd personally like.
- [Uzbl] -- *Web interface tools which adhere to the UNIX philosophy.* The browser had buttons popping out of the window and I couldn't get it to do anything.
- [Vimb] -- *Vim-like browser.*
- [Wyeb] -- *Vim-like WebKit2GTK+ web browser.*

## Chromium

- [Beaker] -- *Browser for the next-generation Web.* -- Chromium but with Dat support.
- [Brave] -- *Secure, fast, and private web browser with adblocker.* Chromium but with pay-to-surf support.
- [Min] -- *Smarter, faster web browser.*
- [Wexond] -- *Extensible web browser with Material UI and built-in ad blocker.*

## TUI

- [ELinks] -- *Full-Featured Text WWW Browser.* Fine.
- [Lynx] -- *Text web browser.* Fine.

## Other

- [Dillo] -- *Multi-platform graphical web browser known for its speed and small footprint.*
- [Firefox] -- *Web browser for X11 derived from the Mozilla browser.*
- [Pale Moon] -- *Independent browser derived from Firefox/Mozilla community code.*

[Beaker]: https://beakerbrowser.com/
[Brave]: https://brave.com/
[Dillo]: https://www.dillo.org
[Elinks]: http://www.elinks.cz/
[Epiphany]: https://wiki.gnome.org/Apps/Web
[Falkon]: https://www.falkon.org/
[Firefox]: https://www.mozilla.org/en-US/firefox/
[Konqueror]: https://konqueror.org/
[Lariza]: https://www.uninformativ.de/git/lariza/file/README.htm
[Luakit]: https://luakit.github.io/
[Lynx]: https://lynx.invisible-island.net
[Min]: https://github.com/minbrowser/min
[Pale Moon]: https://www.palemoon.org
[Qutebrowser]: https://www.qutebrowser.org/
[Surf]: https://surf.suckless.org/
[Surfer]: https://github.com/nihilowy/surfer
[Uzbl]: https://www.uzbl.org/
[Vimb]: https://fanglingsu.github.io/vimb/
[Wexond]: https://github.com/wexond/wexond
[Wyeb]: https://github.com/jun7/wyeb
